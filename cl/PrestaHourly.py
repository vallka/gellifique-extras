#!/home/bitnami/.local/share/virtualenvs/bitnami-957M8Yy0/bin/python

import requests 
import logging
from xml.dom.minidom import parseString

import mypy
from lib.presta_connect import presta_connect

doUpdate = True


connection = presta_connect()

def get_new_orders():

    sql = """
        SELECT id_order,reference,current_state,shipping_number
        FROM ps17_orders o 
        WHERE 
        current_state in (21,4) and
        o.date_upd>DATE_SUB(now(),INTERVAL 1 week)
        """


    with connection.cursor() as cursor:
        cursor.execute(sql)
        result = cursor.fetchall()
        logging.info (result)
        return result

def get_dhl_by_ref(ref):

    # api-endpoint 
    URL = "https://xmlpi-ea.dhl.com/XMLShippingServlet"
    
    xml = """<?xml version="1.0" encoding="UTF-8"?>
            <req:UnknownTrackingRequest xmlns:req="http://www.dhl.com" 
                        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                        xsi:schemaLocation="http://www.dhl.com
                        TrackingRequestUnknown.xsd">
            <Request>
                <ServiceHeader>
                    <SiteID>v62_8aeBy3fri0</SiteID>
                    <Password>96QoWdbB3l</Password>
                </ServiceHeader>
            </Request>
            <LanguageCode>en</LanguageCode>
            <AccountNumber>420760711</AccountNumber>
            <ShipperReference>
                <ReferenceID>{}</ReferenceID>
            </ShipperReference>
            </req:UnknownTrackingRequest>
    """

    xml = xml.format(ref)

    logging.info (xml)
    r = requests.post(url = URL, data = xml) 
    logging.info (r.text)

    try:
        dom = parseString(r.text)
        s = dom.getElementsByTagName('ActionStatus')[0]
        logging.info (s.firstChild.data)
        if s.firstChild.data=='success':
            s = dom.getElementsByTagName('AWBNumber')[0]
            logging.info (s.firstChild.data)

            return s.firstChild.data

    except Exception as exc:
        logging.info(exc)          # __str__ allows args to be printed directly,

    return None

def get_dhl_by_awb(awb):

    # api-endpoint 
    URL = "https://xmlpi-ea.dhl.com/XMLShippingServlet"
    
    xml = """<?xml version="1.0" encoding="UTF-8"?>
            <req:KnownTrackingRequest xmlns:req="http://www.dhl.com" 
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                    xsi:schemaLocation="http://www.dhl.com
                    TrackingRequestKnown.xsd">
                <Request>
                    <ServiceHeader>
                        <SiteID>v62_8aeBy3fri0</SiteID>
                        <Password>96QoWdbB3l</Password>
                    </ServiceHeader>
                </Request>
                <LanguageCode>en</LanguageCode>
                <AWBNumber>{}</AWBNumber>
                <LevelOfDetails>LAST_CHECK_POINT_ONLY</LevelOfDetails>
            </req:KnownTrackingRequest>
    """
    xml = xml.format(awb)

    logging.info (xml)
    r = requests.post(url = URL, data = xml) 
    logging.info (r.text)

    try:
        dom = parseString(r.text)
        ss = dom.getElementsByTagName('EventCode')
        logging.info (ss)
        aa = list(map(lambda x: x.firstChild.data,ss))

        if 'OK' in aa: return 'Delivered'
        if 'PU' in aa: return 'Shipped'
        if 'PL' in aa: return 'Shipped'
        if 'PL' in aa: return 'Shipped'
        if 'AF' in aa: return 'Shipped'
        if 'DF' in aa: return 'Shipped'
        if 'AF' in aa: return 'Shipped'
        if 'PL' in aa: return 'Shipped'
        if 'OH' in aa: return 'Shipped'
        if 'DF' in aa: return 'Shipped'
        if 'AR' in aa: return 'Shipped'
        if 'WC' in aa: return 'Shipped'

    except Exception as exc:
        logging.info(exc)          # __str__ allows args to be printed directly,

    return None

def update_shipping(id,num):
    with connection.cursor() as cursor:
        sql = "update ps17_orders set shipping_number=%s where id_order=%s"
        logging.info (sql % (num, id))
        if doUpdate:
            cursor.execute(sql, (num, id))

        sql = "update ps17_order_carrier set tracking_number=%s where id_order=%s"
        logging.info (sql % (num, id))
        if doUpdate:
            cursor.execute(sql, (num, id))

        if doUpdate:
            connection.commit()

def update_status(id,old_stat,status):
    ns = 0
    if status=='Delivered': ns = 5
    elif status=='Shipped': ns = 4

    if ns and old_stat!=ns:
        with connection.cursor() as cursor:
            sql = "update ps17_orders set current_state=%s where id_order=%s"
            logging.info (sql % (ns, id))
            if doUpdate:
                cursor.execute(sql, (ns,id))

            #dt = dt.replace('T',' ')
            sql = "insert into ps17_order_history (id_employee,id_order,id_order_state,date_add) values (%s,%s,%s,now())"
            logging.info('sql:%s, args:%s,%s,%s',sql,0, id, ns)
            if doUpdate:
                cursor.execute(sql, (0,id,ns))

        if doUpdate:
            connection.commit()
    else:
        logging.info ("not updated -- ns: %s, os: %s",ns,old_stat)



    logging.info ('Done')
    connection.commit()

try:
    oo = get_new_orders()

    for o in oo:
        #print (o['reference'],o['shipping_number'])
        if o['shipping_number']:
            awb = o['shipping_number']
        else:    
            awb = get_dhl_by_ref(o['reference'])
        
        if awb:
            stat = get_dhl_by_awb(awb)
            logging.info (stat)

            if stat:
                # only when it's at least  picked up
                if not o['shipping_number']:
                    update_shipping(o['id_order'],awb)

                update_status(o['id_order'],o['current_state'],stat)



except Exception as exc:
    logging.error(type(exc))    # the exception instance
    logging.error(exc)          # __str__ allows args to be printed directly,

finally:
   connection.close()


