#!/home/bitnami/.local/share/virtualenvs/bitnami-957M8Yy0/bin/python

import logging
import mypy
from lib.presta_connect import presta_connect

import os
from datetime import datetime
import re


doUpdate = True
connection = presta_connect()



def updateStats(date_add,orders,products,single_products,single_products_on_sale,gbp_products,gbp_products_on_sale):
    if doUpdate:
        with connection.cursor() as cursor:
            usql = """
insert into a_order_stats (date_add,orders,products,single_products,single_products_on_sale,gbp_products,gbp_products_on_sale) values
(%s,%s,%s,%s,%s,%s,%s)
on duplicate key update
orders=%s,
products=%s,
single_products=%s,
single_products_on_sale=%s,
gbp_products=%s,
gbp_products_on_sale=%s
"""
            cursor.execute(usql, (date_add,orders,products,single_products,single_products_on_sale,gbp_products,gbp_products_on_sale,orders,products,single_products,single_products_on_sale,gbp_products,gbp_products_on_sale))

        connection.commit()


try:
    sql="""

    select 
    date_add,
    count(id_order) orders,
    sum(products) products,
    sum(single_products) single_products,
    sum(single_products_on_sale) single_products_on_sale,
    sum(GBP_products) GBP_products,
    sum(GBP_products_on_sale) GBP_products_on_sale
    from
    (
    select substr(date_add,1,10) date_add, 
    id_order, 
    (select count(*) from ps17_order_detail d where id_order=o.id_order) products, 
    (SELECT sum(if(unity>=1,unity,1)) FROM `ps17_order_detail` oo join ps17_product p on p.id_product=oo.product_id where id_order=o.id_order) single_products, 
    (SELECT sum(if(unity>=1,unity,1)) FROM `ps17_order_detail` oo join ps17_product p on p.id_product=oo.product_id and on_sale=1 where id_order=o.id_order) single_products_on_sale, 
    total_products_wt/conversion_rate GBP_products,
    (SELECT sum(total_price_tax_incl) FROM `ps17_order_detail` oo join ps17_product p on p.id_product=oo.product_id and on_sale=1 where id_order=o.id_order)/conversion_rate 
    GBP_products_on_sale
    FROM `ps17_orders` o where current_state in (2,3,4,5,17) 
    )ooo

    where date_add>=DATE_SUB(date(now()), INTERVAL 1 DAY)

    group by date_add
    """
    with connection.cursor() as cursor:
        cursor.execute(sql)
        result = cursor.fetchall()

        #print( result)

    if result:
        for r in result:
            #print (r['date_add'])    
            if r['orders']==None:
                r['orders']=0
            if r['products']==None:
                r['products']=0
            if r['single_products']==None:
                r['single_products']=0
            if r['single_products_on_sale']==None:
                r['single_products_on_sale']=0
            if r['GBP_products']==None:
                r['GBP_products']=0
            if r['GBP_products_on_sale']==None:
                r['GBP_products_on_sale']=0

            logging.info ('Date: %s %s %s %s %s %s %s',r['date_add'],r['orders'],r['products'],r['single_products'],r['single_products_on_sale'],r['GBP_products'],r['GBP_products_on_sale'])
            updateStats(r['date_add'],r['orders'],r['products'],r['single_products'],r['single_products_on_sale'],r['GBP_products'],r['GBP_products_on_sale'])

except Exception as inst:
    logging.error(type(inst))    # the exception instance
    logging.error(inst)          # __str__ allows args to be printed directly,

finally:
   connection.close()

