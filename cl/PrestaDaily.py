#!/home/bitnami/.local/share/virtualenvs/bitnami-957M8Yy0/bin/python

import logging
import mypy
from lib.presta_connect import presta_connect

doUpdate = False


connection = presta_connect()

####################################################################################
# update_latest_arrivals
####################################################################################
def get_ps_nb_days_new_product():
    sql="select value FROM `ps17_configuration` o where name='PS_NB_DAYS_NEW_PRODUCT'"

    with connection.cursor() as cursor:
        cursor.execute(sql)
        result = cursor.fetchall()
        logging.info (result)
        return result[0]['value']


def update_latest_arrivals(days):
    sql="""
        delete from ps17_category_product 
        where id_category=94 and 
        id_product not in (SELECT id_product FROM ps17_product where date_add>DATE_SUB(now(),INTERVAL %s day) and active=1)
    """
    with connection.cursor() as cursor:
        logging.info (sql % days)
        cursor.execute(sql,days)

    sql="""
        insert ignore into ps17_category_product (id_category,id_product)
        select 94,id_product FROM ps17_product where date_add>DATE_SUB(now(),INTERVAL %s day) and active=1
    """
    with connection.cursor() as cursor:
        logging.info (sql % days)
        cursor.execute(sql,days)

    logging.info ('Done')
    connection.commit()


####################################################################################
# trustpilot
####################################################################################
def add_review(dt,name,url,heading,body,rating):
    sql="""
        insert ignore into a_tp_review(dt,name,url,headline,body,rating)
        values (%s,%s,%s,%s,%s,%s)
    """
    with connection.cursor() as cursor:
        logging.info (sql % (dt,name,url,heading,body,rating))
        cursor.execute(sql,(dt,name,url,heading,body,rating))

    logging.info ('Done')
    connection.commit()


def get_tp_reviews():
    import requests 
    import re
    import json
    
    url = "https://www.trustpilot.com/review/gellifique.co.uk"

    r = requests.get(url)
    m = re.search(r'<script type="application\/ld\+json" data-business-unit-json-ld>\s*(.+?)\s*<\/script>', r.text,re.DOTALL)
    obj = json.loads(m.group(1))
    reviews = obj[0]['review']
    reviews.reverse()

    for r in reviews:
        #print (r['datePublished'],r['author']['name'],r['author']['url'],r['headline'],r['reviewBody'],r['reviewRating']['ratingValue'])
        add_review(r['datePublished'],r['author']['name'],r['author']['url'],r['headline'],r['reviewBody'],r['reviewRating']['ratingValue'])

    logging.info (len(reviews))
    #logging.info (reviews)


####################################################################################
####################################################################################
## MAIN
####################################################################################
try:
    days = get_ps_nb_days_new_product()
    update_latest_arrivals(days)

    get_tp_reviews()

except Exception as exc:
    logging.error(type(exc))    # the exception instance
    logging.error(exc)          # __str__ allows args to be printed directly,

finally:
   connection.close()


