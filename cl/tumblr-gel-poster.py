#!/home/bitnami/.local/share/virtualenvs/bitnami-957M8Yy0/bin/python

import os
from datetime import datetime
import mypy
from lib.presta_connect import presta_connect

import logging

#os.environ['TZ'] = 'Europe/London'
#logging.info ('\n' + str(datetime.now().strftime("%Y-%m-%d %X")) + ' *** Start ***')

import requests
import pymysql.cursors
import re
import random
import pytumblr
from urllib.request import urlopen


# Authenticate via OAuth
client = pytumblr.TumblrRestClient(
  'us7DGxHjrZlCJFHkz1JH3DFAp3TC0fdTGqS1ZCscLwJ3tdCyiH',
  'kZOpLaq7BQWvQlRASrPvpxD0aVOMQ9qoIl26QceGArVFdyPkXn',
  'ofMo8Xr9Z54IFBp5fczUboZ2K8WZKBf6sRLOzpkue40ju2eI67',
  'XKJSouSAbrplD7aWDdgIffCmw368mbDndHY6aiXrcEk34wvz0d'
)

# Make the request
#logging.info (client.info())

# Connect to the database
connection = presta_connect('dj')


def getInsta(conn,news):
    if news:
        logging.info ('getting new...')
        sql = """
SELECT username,code,caption,products FROM `instagrab_gellifique_gel_colour` i
            WHERE username in ('gellifique_gel_colour','rusea.nail.art')
            and created_dt>DATE_ADD(now(), INTERVAL -1 DAY)
            and not exists (
            select id from tumblr_gellifique_gel_colour where source=concat('IG:',i.code)
            )
            order by taken_at
            limit 0,1
"""
    else:
        logging.info ('getting old...')
        sql = """
SELECT username,code,caption,products FROM `instagrab_gellifique_gel_colour` i
            WHERE username in ('gellifique_gel_colour','rusea.nail.art')
            and created_dt<=DATE_ADD(now(), INTERVAL -1 DAY)
            and taken_at>=DATE_ADD(now(), INTERVAL -1 year)
            and not exists (
            select id from tumblr_gellifique_gel_colour where source=concat('IG:',i.code)
            )
"""

    with connection.cursor() as cursor:
        cursor.execute(sql)
        result = cursor.fetchall()

    if news:
        return result
    else:
        #return [random.choice(result)]
        return random.sample(result,1)


def PostIt(client,text,tags,picurl):
    blogName = 'gellifique.tumblr.com'
    r = client.create_photo(blogName, state="published", tags=tags, source=picurl, caption=text)

    logging.info(r)
    twidid = r['id']
    
    return twidid

def updateDb(connection,tweet_id,ig_code,name):
    with connection.cursor() as cursor:
        source = 'IG:'+ig_code
        name = name.encode("ascii","ignore").decode("utf-8","ignore")
        sql = "insert into tumblr_gellifique_gel_colour (name,tumblr_id,source) values (%s,%s,%s)"
        cursor.execute(sql, (str(name), str(tweet_id), str(source)))

    connection.commit()
    logging.info('logged to db')

# begin

try:
    instas = getInsta(connection,True)

    if not instas:
        instas = getInsta(connection,False)

    if instas:
        logging.info('**gotit!')

        for i in instas:
            #logging.info (i['code'],i['caption'],i['products'])
            prod = re.sub(',.*$','',i['products'])
            picurl = 'https://www.instagram.com/p/'+i['code']+'/media/?size=l'
            logging.info ('%s %s %s %s %s',i['code'],i['username'],i['products'],prod,picurl)

            if prod:
                url = "https://www.gellifique.co.uk/index.php?controller=product&id_product="+prod
            else:
                url = "https://www.gellifique.co.uk"

            #logging.info (i['caption'])
            tags = re.findall('#\w+',i['caption'])
            text = re.sub('#\w+','',i['caption'])

            text = re.sub('————+',"———",text)
            text = re.sub('____+',"———",text)


            if i['username']!='gellifique_gel_colour':
                text = 'Best works of our partners: https://www.instagram.com/' + i['username'] + '\n\n' +text

            text += "\n\nShop now:\n" + url

            #text = text.encode("ascii","ignore").decode("utf-8","ignore")

            #logging.info ('===>',text,'<===')
            tweet_id = PostIt(client,text,tags,picurl)
            logging.info ('tweet_id:'+str(tweet_id))

            updateDb(connection,tweet_id,i['code'],text)

finally:
    connection.close()
    #logging.info (str(datetime.now().strftime("%Y-%m-%d %X")) + ' *** End ***\n')
