#!/homepages/28/d116680254/htdocs/.virtualenvs/env1/bin/python3

##          #!/usr/bin/python3



from flup.server.fcgi import WSGIServer
from api import app

#import sys
#sys.path.insert(0, '/homepages/28/d116680254/htdocs/.virtualenvs/env1/lib/python3.4/site-packages')


class ScriptNameStripper(object):
   def __init__(self, app):
       self.app = app

   def __call__(self, environ, start_response):
       environ['SCRIPT_NAME'] = '/apipy'
       return self.app(environ, start_response)

app = ScriptNameStripper(app)


if __name__ == '__main__':
    WSGIServer(app).run()