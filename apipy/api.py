#!/home/bitnami/.local/share/virtualenvs/myflask-3YVQhxhy/bin/python


##!/usr/bin/python3

from flask import Flask
from flask_restplus import fields, Resource, Api
import pymysql.cursors
import re

app = Flask(__name__)
api = Api(app)

name_space = api.namespace('orders', description='Orders')

modelOrder = api.model('Order', {
	"id_order": fields.Integer(readOnly=True, description='Order unique identifier'),
    "reference": fields.String(readOnly=True, description='Order unique string reference'),
	"current_state": fields.Integer,
	"order_state": fields.String,
    "shipping_number": fields.String,
    "firstname": fields.String,
    "lastname": fields.String,
    "note": fields.String,
    "firstname_a": fields.String,
    "lastname_a": fields.String,
    "email": fields.String,
    "postcode": fields.String,
    "address1": fields.String,
    "address2": fields.String,
    "city": fields.String,
    "phone": fields.String,
    "country": fields.String,
    "id_currency": fields.Integer,
    "total_paid": fields.String,
    "total_products_wt": fields.String,
    "total_shipping_tax_incl": fields.String,
	"date_add": fields.String,
	"date_upd": fields.String,
    "id_customer": fields.Integer,
    "id_address_delivery": fields.Integer,
    "id_country": fields.Integer,
	"carrier": fields.String,
    "new": fields.Integer,
})

modelDetails = api.inherit('OrderDetails',modelOrder, {
	"items": fields.Nested(api.model('OrderItems',{
		"product_name": fields.String,
		"product_reference": fields.String,
	    "product_id": fields.Integer,
	    "product_quantity": fields.Integer,
		"unit_price_tax_incl": fields.String,
		"total_price_tax_incl": fields.String,
	    "id_image": fields.Integer,
	    "quantity": fields.Integer,
	    "physical_quantity": fields.Integer,
	    "unity": fields.Integer,
	}))
})

@name_space.route("/")
class OrdersList(Resource):
	@api.marshal_with(modelOrder)
	def get(self):
		try:
			app.logger.info('OrdersList::get')
			return getOrders()
		except:
			app.logger.exception('OrdersList::get::exception')

@name_space.route("/<int:id>")
class OrderDetails(Resource):
	@api.marshal_with(modelDetails)
	def get(self,id):
		try:
			app.logger.info('OrderDetails::get')
			return getOrders(id)
		except:
			app.logger.exception('OrderDetails::get::exception')


def getOrders(id=None):
	# Connect to the database
	connection = pymysql.connect(host='db5000223647.hosting-data.io',
                             user='dbu40791',
                             password='Dobroskokina2&',
                             db='dbs218339',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)


	sql = 	"""
			SELECT id_order,reference,o.current_state
			,(select name from ps17_order_state_lang where id_lang=1 and id_order_state=o.current_state) order_state
			,o.shipping_number
			,c.firstname,c.lastname,c.note
            ,a.firstname as firstname_a,a.lastname as lastname_a
            ,c.email,a.postcode,a.address1,a.address2,a.city,a.phone
			,(select name from ps17_country_lang where id_country=a.id_country) country
			,id_currency,total_paid ,total_products_wt ,total_shipping_tax_incl,o.date_add,o.date_upd
			,o.id_customer,o.id_address_delivery,a.id_country
            ,ca.name as carrier
            ,IF((SELECT so.id_order FROM `ps17_orders` so WHERE so.id_customer = o.id_customer AND so.id_order < o.id_order LIMIT 1) > 0, 0, 1) as new
			FROM `ps17_orders` o
			JOIN ps17_customer c on o.id_customer=c.id_customer
			JOIN ps17_address a on id_address_delivery=a.id_address
            join ps17_carrier ca on ca.id_carrier=o.id_carrier
			__WHERE__
			ORDER BY id_order DESC
			limit 0,100
			"""

	if id==None:
		sql = re.sub(r'__WHERE__','',sql)
	else:
		sql = re.sub(r'__WHERE__','WHERE id_order=%s',sql)

	with connection.cursor() as cursor:
		cursor.execute(sql,(id))
		result = cursor.fetchall()

	if id!=None:
		result[0]['items'] = getOrderItems(connection,id)
		

	return result

def getOrderItems(connection,id):
	sql = """
	SELECT p.* ,a.quantity,a.physical_quantity,i.id_image
	,if(pr.unity>1,pr.unity,1) unity
	FROM ps17_order_detail p 
	left outer join ps17_image i on p.product_id=i.id_product and i.cover=1 
	left outer join ps17_stock_available a on a.id_product=p.product_id and 	
	a.id_product_attribute=p.product_attribute_id
    left outer join ps17_product pr on pr.id_product=p.product_id	
	WHERE id_order=%s order by product_name
	"""

	with connection.cursor() as cursor:
		cursor.execute(sql,(id))
		result = cursor.fetchall()

	return result


#print ('running')
#out = executeShell(['~/cl/ParseParcel.py'])


#print (json.dumps(obj))


name_space2 = api.namespace('hermes', description='Hermes Parcels')

modelParcel = api.model('Parcel', {
	"Address_line_1": fields.String,
	"Address_line_2": fields.String,
	"Address_line_3": fields.String,
	"Address_line_4": fields.String,
	"Postcode": fields.String,
	"First_name": fields.String,
	"Last_name": fields.String,
	"Email": fields.String,
	"Weight": fields.String,
	"Compensation": fields.String,
	"Signature": fields.String,
	"Reference": fields.String,
	"Contents": fields.String,
	"Parcel_value": fields.String,
	"Delivery_phone": fields.String,
	"Delivery_safe_place": fields.String,
	"Delivery_instructions": fields.String,
	"Service": fields.String,
})

@name_space2.route("/")
class HermesList(Resource):
	@api.marshal_with(modelParcel)
	def get(self):
		try:
			app.logger.info('HermesList::get')
			return getHermes()
		except:
			app.logger.exception('OrdersList::get::exception')

def getHermes():
	"""
	return [{
			"Address_line_1": 'fields.String',
			"Address_line_2": 'fields.String',
			"Address_line_3": 'fields.String',
			"Address_line_4": 'fields.String',
			"Postcode": 'fields.String',
			"First_name": 'fields.String',
			"Last_name": 'fields.String',
			"Email": 'fields.String',
			"Weight": 'fields.String',
			"Compensation": 'fields.String',
			"Signature": 'fields.String',
			"Reference": 'fields.String',
			"Contents": 'fields.String',
			"Parcel_value": 'fields.String',
			"Delivery_phone": 'fields.String',
			"Delivery_safe_place": 'fields.String',
			"Delivery_instructions": 'fields.String',
			"Service": 'fields.String'
		}]
	"""

	# Connect to the database
	connection = pymysql.connect(host='db5000223647.hosting-data.io',
                             user='dbu40791',
                             password='Dobroskokina2&',
                             db='dbs218339',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)


	sql = 	"""
			SELECT 
			if (a.company!='',a.company,a.address1) Address_line_1
			,if (a.company!='',a.address1,COALESCE(a.address2,'')) Address_line_2
			,if (a.company!='',COALESCE(a.address2,''),'') Address_line_3
			,a.city  Address_line_4
			,a.postcode Postcode
			,a.firstname First_name
			,a.lastname Last_name
			,c.email Email
			,(select sum(product_weight*product_quantity) from ps17_order_detail d where d.id_order=o.id_order) Weight
			,'' Compensation
			,'y' Signature
			,reference Reference
			,'' Contents
			, '' Parcel_value
			,a.phone Delivery_phone
			,'' Delivery_safe_place
			,'' Delivery_instructions
			,'' Service
			FROM ps17_orders o
			join ps17_address a on a.id_address=o.id_address_delivery and a.id_country=17
			join ps17_customer c on o.id_customer=c.id_customer
			WHERE current_state=17
			and id_carrier=134
			ORDER BY o.id_order  
			"""

	with connection.cursor() as cursor:
		cursor.execute(sql)
		result = cursor.fetchall()

	return result

name_space4 = api.namespace('DHL', description='DHL Parcels')

modelDHLParcel = api.model('Parcel', {
	"name_ship_from": fields.String,
	"company_ship_from": fields.String,
	"address_1_ship_from": fields.String,
	"address_2_ship_from": fields.String,
	"address_3_ship_from": fields.String,
	"house_number_ship_from": fields.String,
	"postal_code_ship_from": fields.String,
	"city_ship_from": fields.String,
	"country_code_ship_from": fields.String,
	"email_address_ship_from": fields.String,
	"phone_country_code_ship_from": fields.String,
	"phone_number_ship_from": fields.String,
	"name_ship_to": fields.String,
	"company_ship_to": fields.String,
	"address_1_ship_to": fields.String,
	"address_2_ship_to": fields.String,
	"address_3_ship_to": fields.String,
	"house_number_ship_to": fields.String,
	"postal_code_ship_to": fields.String,
	"city_ship_to": fields.String,
	"state_code_ship_to": fields.String,
	"country_code_ship_to": fields.String,
	"email_address_ship_to": fields.String,
	"phone_country_code_ship_to": fields.String,
	"phone_number_ship_to": fields.String,
	"account_number_shipper": fields.String,
	"total_weight": fields.String,
	"declared_value_currency": fields.String,
	"declared_value": fields.String,
	"product_code_3_letter": fields.String,
	"summary_of_contents": fields.String,
	"shipment_type": fields.String,
	"shipment_reference": fields.String,
	"total_shipment_pieces": fields.String,
	"invoice_type": fields.String,
	"length": fields.String,
	"width": fields.String,
	"depth": fields.String,
})

@name_space4.route("/")
class DhlList(Resource):
	@api.marshal_with(modelDHLParcel)
	def get(self):
		try:
			app.logger.info('DhlList::get')
			return getDhl()
		except:
			app.logger.exception('OrdersList::get::exception')

def getDhl():

	# Connect to the database
	connection = pymysql.connect(host='db5000223647.hosting-data.io',
                             user='dbu40791',
                             password='Dobroskokina2&',
                             db='dbs218339',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)


	"""
	SELECT
				'Larisa Dobroskokina' name_ship_from,
				'GellifiQue Ltd' company_ship_from,
				'159 Great Junction Street' address_1_ship_from,
				'' address_2_ship_from,
				'' address_3_ship_from,
				'' house_number_ship_from,
				'EH6 5LG' postal_code_ship_from,
				'Edinburgh' city_ship_from,
				'GB' country_code_ship_from,
				'info@gellifique.com' email_address_ship_from,
				'44' phone_country_code_ship_from,
				'7729192470' phone_number_ship_from,
				concat(a.firstname,' ',a.lastname) name_ship_to,
				if (a.company!='',a.company,concat(a.firstname,' ',a.lastname)) company_ship_to,
				COALESCE(a.address1,'') address_1_ship_to,
				COALESCE(a.address2,'') address_2_ship_to,
				'' address_3_ship_to,
				'' house_number_ship_to,
				COALESCE(a.postcode,'') postal_code_ship_to,
				COALESCE(a.city,'') city_ship_to,
				COALESCE(if (a.id_country!=17,(select iso_code from ps17_state where id_state=a.id_state),''),'') state_code_ship_to,
				(select iso_code from ps17_country where id_country=a.id_country) country_code_ship_to,
				c.email email_address_ship_to,
				(select call_prefix from ps17_country where id_country=a.id_country) phone_country_code_ship_to,
				a.phone phone_number_ship_to,
				'420760711' account_number_shipper,
				(select sum(product_weight*product_quantity) from ps17_order_detail d where d.id_order=o.id_order) total_weight,
				(select iso_code from ps17_currency where id_currency=o.id_currency) declared_value_currency,
				if (a.id_country!=17,(o.total_paid_real-o.total_shipping_tax_incl),0) declared_value,
				if (a.id_country!=17,'WPX','DOM') product_code_3_letter,
				'Manicure Accessories' summary_of_contents,
				'P' shipment_type,
				o.reference shipment_reference,
				'1' total_shipment_pieces,
				if (a.id_country!=17,'PRO','') invoice_type,
				20 length,
				15 width,
				10 depth
			FROM ps17_orders o
				join ps17_address a on a.id_address=o.id_address_delivery
				join ps17_customer c on o.id_customer=c.id_customer
			WHERE 
				id_carrier in (177,174)
				and 
				current_state=21
				ORDER BY o.id_order
	"""



	sql = 	"""
			SELECT
				'Larisa Dobroskokina' name_ship_from,
				'GellifiQue Ltd' company_ship_from,
				'41 Deantown Avenue' address_1_ship_from,
				'' address_2_ship_from,
				'' address_3_ship_from,
				'' house_number_ship_from,
				'EH21 8NS' postal_code_ship_from,
				'Musselburgh' city_ship_from,
				'GB' country_code_ship_from,
				'info@gellifique.com' email_address_ship_from,
				'44' phone_country_code_ship_from,
				'7729192470' phone_number_ship_from,
				concat(a.firstname,' ',a.lastname) name_ship_to,
				if (a.company!='',a.company,concat(a.firstname,' ',a.lastname)) company_ship_to,
				COALESCE(a.address1,'') address_1_ship_to,
				COALESCE(a.address2,'') address_2_ship_to,
				'' address_3_ship_to,
				'' house_number_ship_to,
				COALESCE(a.postcode,'') postal_code_ship_to,
				COALESCE(a.city,'') city_ship_to,
				if (a.id_country!=17,(select iso_code from ps17_state where id_state=a.id_state),'') state_code_ship_to,
				(select iso_code from ps17_country where id_country=a.id_country) country_code_ship_to,
				c.email email_address_ship_to,
				(select call_prefix from ps17_country where id_country=a.id_country) phone_country_code_ship_to,
				a.phone phone_number_ship_to,
				'420760711' account_number_shipper,
				(select sum(product_weight*product_quantity) from ps17_order_detail d where d.id_order=o.id_order) total_weight,
				(select iso_code from ps17_currency where id_currency=o.id_currency) declared_value_currency,
				if (a.id_country!=17,(o.total_paid_real-o.total_shipping_tax_incl),0) declared_value,
				if (a.id_country!=17,'WPX','DOM') product_code_3_letter,
				'Manicure Accessories' summary_of_contents,
				'P' shipment_type,
				o.reference shipment_reference,
				'1' total_shipment_pieces,
				if (a.id_country!=17,'PRO','') invoice_type,
				20 length,
				15 width,
				10 depth
			FROM ps17_orders o
				join ps17_address a on a.id_address=o.id_address_delivery
				join ps17_customer c on o.id_customer=c.id_customer
			WHERE 
				id_carrier in (177,174)
				and 
				current_state=21
				ORDER BY o.id_order			"""

	with connection.cursor() as cursor:
		cursor.execute(sql)
		result = cursor.fetchall()

	return result



name_space3 = api.namespace('chatbot', description='chatbot API')

@name_space3.route("/<string:q>")
class ChatBot(Resource):
	def get(self,q):
		try:
			app.logger.info('chatbot::get')
			return getResponse(q)
		except:
			app.logger.exception('ChatBot::get::exception')

def detect_intent_texts(project_id, session_id, texts, language_code):
	import dialogflow_v2 as dialogflow
	import os
	os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/homepages/28/d116680254/htdocs/cl/__secret/GellifiQue Project-b21611e2b6f6.json" 

	session_client = dialogflow.SessionsClient()
	
	session = session_client.session_path(project_id, session_id)
	#print('Session path: {}\n'.format(session))

	text_input = dialogflow.types.TextInput(text=texts, language_code=language_code)

	query_input = dialogflow.types.QueryInput(text=text_input)

	try:
		response = session_client.detect_intent(session=session, query_input=query_input)
	except Exception as e:
		return "Exception"
		#print (e)

	#return "123"

	return response.query_result.fulfillment_text

""" 	print('=' * 20)
	print('Query text: {}'.format(response.query_result.query_text))
	print('Detected intent: {} (confidence: {})\n'.format(
		response.query_result.intent.display_name,
		response.query_result.intent_detection_confidence))

	txt =     response.query_result.fulfillment_text
	#print('Fulfillment text: {}\n'.format(txt))
	print('Fulfillment text: ',txt)
	return txt
 """

def getResponse(q):
	txt = detect_intent_texts('gellifique-project', '12345', q, 'en-US')
	return {"resonse_text": "You said '"+txt+"'"}


name_space5 = api.namespace('TPReviews', description='TP Reviews')

modelTPReviews = api.model('Parcel', {
	"id": fields.Integer,
	"dt": fields.String,
	"name": fields.String,
	"url": fields.String,
	"headline": fields.String,
	"body": fields.String,
	"rating": fields.Integer,
})

@name_space5.route("/")
class TPReviews(Resource):
	@api.marshal_with(modelTPReviews)
	def get(self):
		try:
			app.logger.info('TPReviews::get')

			return getTPReviews()
		except:
			app.logger.exception('TPReviews::get::exception')

def getTPReviews():

	from flask_restplus import reqparse
	import random

	parser = reqparse.RequestParser()
	parser.add_argument('len', type=int)
	parser.add_argument('lenmin', type=int)
	parser.add_argument('lenmax', type=int)
	parser.add_argument('seed', type=int)
	args = parser.parse_args()	

	len = args.get('len',0)
	lenmin = args.get('lenmin',0)
	lenmax = args.get('lenmax',0)
	seed  = args.get('seed',0)

	# Connect to the database
	connection = pymysql.connect(host='db5000223647.hosting-data.io',
                             user='dbu40791',
                             password='Dobroskokina2&',
                             db='dbs218339',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)


	sql = 	"""
			SELECT * FROM a_tp_review order by dt desc
			"""

	with connection.cursor() as cursor:
		cursor.execute(sql)
		result = cursor.fetchall()

	if seed: random.seed(seed)

	if not len and lenmin and lenmax: len = random.randint(lenmin,lenmax)
	
	if len:
		result = random.sample(result,len)
		result.sort(key=lambda rev: rev['id'])

	return result





if __name__ == '__main__':
	#print (getInsta())
	app.run(debug=True)
