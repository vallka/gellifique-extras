#!/usr/bin/python3

from flask import Flask,render_template

app=Flask(__name__)

@app.route('/')
def home():
    return render_template("home.html")

@app.route('/about')
def about():
    return render_template("about.html")

if __name__=="__main__":
    app.run(debug=True)


#"""
#SELECT id_order ,reference ,o.id_customer ,o.id_address_delivery ,total_paid ,total_products_wt ,total_shipping_tax_incl ,c.firstname ,c.lastname ,c.email ,a.address1 ,a.address2 ,a.postcode ,a.city ,a.phone ,a.id_country 
#,(select name from ps17_country_lang where id_country=a.id_country) country
#FROM `ps17_orders` o 
#JOIN ps17_customer c on o.id_customer=c.id_customer 
#JOIN ps17_address a on id_address_delivery=a.id_address 
#WHERE id_order>=220
#"""